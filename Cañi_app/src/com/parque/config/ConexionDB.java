
package com.parque.config;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConexionDB {
    
    //abre  la conexion  a la DB
    public static Connection obtenerConexion() throws ClassNotFoundException, SQLException{
    
        return ApacheDerbyConnection.getApacheDerbyConnection();
    }
    //cierra la conexion a la DB
    public static void cerrarConexion(Connection conn){
    
        try {
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error sql (cerrando conexion)---"+ex.getMessage());
        }
    
    }
    
    public static void rollback(Connection conn){
    
        try {
            conn.rollback();
        } catch (SQLException exr) {
            System.out.println("Eroor en el sql (rollback)--:"+exr.getMessage());
        }
    
    }
    
    
    public static void commit(Connection conn){
    
        try {
            conn.commit();
        } catch (SQLException ex) {
            System.out.println("Confimado el bloque transaccion SQL");
        }
    
    }
    
    
    
    
    
}
