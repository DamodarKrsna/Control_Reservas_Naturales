package com.parque.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApacheDerbyConnection {

    //metodo que contiene los parametros de  conexion apacheAerby DB
    public static Connection getApacheDerbyConnection() throws ClassNotFoundException, SQLException {
        //parametros para la conexion
        String hostName = "localhost";
        String puerto = "1527";
        String dbName = "cani";
        String userName = "cani";
        String usePassword = "cani";
        String protocolo = "jdbc:derby://";
        return getApacheDerbyConnection(hostName, puerto, dbName, userName, usePassword, protocolo);
    }
    //metodo que carga el driver de la Db y establese la conexion a la DB
    public static Connection getApacheDerbyConnection(String hostName, String puerto,String dbName, String userName, String usePassword,String protocolo) throws ClassNotFoundException, SQLException {
        //Prepara el driver para ser utilisado con el metodo class.forName
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        //la url que cargara el metodo getConection para la conexion a la db
        String connectionURL = protocolo+hostName+ ":"+puerto+"/"+dbName;
        //cargamos la conexion con el metodo getConnection
        Connection conn = DriverManager.getConnection(connectionURL,userName, usePassword);
        
        return conn;
    }
    
//
//    public static void main (String[] args) throws SQLException{   
//        Connection conn = null;
//        
//        try {
//  
//            conn = ConexionDB.obtenerConexion();
//          
//        } catch (ClassNotFoundException ex) {
//            System.out.println("Error sql conxion---:"+ex.getMessage());
//        }
//        
//        System.out.println("Conexion----:"+conn);
//        
//        
//    }
    
    
 
}
