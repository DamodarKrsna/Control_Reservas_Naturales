package com.parque.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.parque.config.ConexionDB;

public class UsuarioDAO {

    //metodo recibe un usuario y contraseña, y verifica si esos 2 parametros
    //estan en la base de datos.Si se encuentran devuelve true si no false
    public boolean autentificarUsuario(String usuario, String contrasena) {

        //creo un objeto conexion de la interfaz connection
        Connection conn = null;
        //si la variable es true(usuario contraseña coinciden) si es false (no coinciden)           
        boolean autentification = false;
        try {

            //establesco la coneccion
            conn = ConexionDB.obtenerConexion();
            //las consultas seran divididas
            conn.setAutoCommit(false);
            String consultaUsuario = "Select * from USUARIO";
            //ejecutaremos la consulta objeto preparedStatement
            PreparedStatement st;

            ResultSet rs;

            st = conn.prepareStatement(consultaUsuario);
            rs = st.executeQuery();

            while (rs.next()) {
                if (usuario.equals(rs.getString("Usuario")) && contrasena.equals(rs.getString("Password"))) {
                    autentification = true;
                }
            }
            
            //este metodo da OK, salido sin errores.Es metodo de la interfaz connection
            conn.commit();

        } catch (SQLException ex2) {
            System.out.println("SQL error--" + ex2);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error de conexio--:" + ex);
        }

        return autentification;

    }

    
//     public static void main(String[] args) throws SQLException {
//        UsuarioDAO con = new UsuarioDAO();
//        System.out.println(con.Autenticacion("hackro", "ww"));
//    }
    
    
    
    
    
    
    
    
    
    
}
