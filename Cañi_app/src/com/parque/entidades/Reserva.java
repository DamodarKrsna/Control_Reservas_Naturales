
package com.parque.entidades;


public class Reserva {
    
    private int idReserva;
    private String reservaCheckin;
    private String reservaChackaut;
    private int reservaNochesEstadia;
    private boolean reservaGuiado;
    private boolean reservaTemporada;

    public Reserva(int idReserva, String reservaCheckin, String reservaChackaut, int reservaNochesEstadia, boolean reservaGuiado, boolean reservaTemporada) {
        this.idReserva = idReserva;
        this.reservaCheckin = reservaCheckin;
        this.reservaChackaut = reservaChackaut;
        this.reservaNochesEstadia = reservaNochesEstadia;
        this.reservaGuiado = reservaGuiado;
        this.reservaTemporada = reservaTemporada;
    }

    public int getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public String getReservaCheckin() {
        return reservaCheckin;
    }

    public void setReservaCheckin(String reservaCheckin) {
        this.reservaCheckin = reservaCheckin;
    }

    public String getReservaChackaut() {
        return reservaChackaut;
    }

    public void setReservaChackaut(String reservaChackaut) {
        this.reservaChackaut = reservaChackaut;
    }

    public int getReservaNochesEstadia() {
        return reservaNochesEstadia;
    }

    public void setReservaNochesEstadia(int reservaNochesEstadia) {
        this.reservaNochesEstadia = reservaNochesEstadia;
    }

    public boolean isReservaGuiado() {
        return reservaGuiado;
    }

    public void setReservaGuiado(boolean reservaGuiado) {
        this.reservaGuiado = reservaGuiado;
    }

    public boolean isReservaTemporada() {
        return reservaTemporada;
    }

    public void setReservaTemporada(boolean reservaTemporada) {
        this.reservaTemporada = reservaTemporada;
    }
    
    
   
    
    
    
    
    
    
    
}
