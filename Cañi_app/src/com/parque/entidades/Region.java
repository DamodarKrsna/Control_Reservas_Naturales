
package com.parque.entidades;


public class Region {
    
    private int idRegion;
    private String nombreRegion;
    private int numeroRegion;

    public Region(int idRegion, String nombreRegion, int numeroRegion) {
        this.idRegion = idRegion;
        this.nombreRegion = nombreRegion;
        this.numeroRegion = numeroRegion;
    }

    public int getNumeroRegion() {
        return numeroRegion;
    }

    public void setNumeroRegion(int numeroRegion) {
        this.numeroRegion = numeroRegion;
    }

    public int getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(int idRegion) {
        this.idRegion = idRegion;
    }

    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }
    
   
    
    
}
