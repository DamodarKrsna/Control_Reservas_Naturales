
package com.parque.entidades;


public class Calle {
    private int idCalle;
    private String nombreCalle;
    private int calleNumero;

    public Calle(int idCalle, String nombreCalle, int calleNumero) {
        this.idCalle = idCalle;
        this.nombreCalle = nombreCalle;
        this.calleNumero = calleNumero;
    }

    public int getIdCalle() {
        return idCalle;
    }

    public void setIdCalle(int idCalle) {
        this.idCalle = idCalle;
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public void setNombreCalle(String nombreCalle) {
        this.nombreCalle = nombreCalle;
    }

    public int getCalleNumero() {
        return calleNumero;
    }

    public void setCalleNumero(int calleNumero) {
        this.calleNumero = calleNumero;
    }
    
    
 
    
    
}
