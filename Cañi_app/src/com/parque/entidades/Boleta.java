
package com.parque.entidades;


public class Boleta {
    
    private int idBoleta;
    private float boletaNeto;
    private float boletaImpuesto;
    private float boletaTotal;
    private boolean boletaFormal;
    private String boletaFechaModificacion;

    public Boleta(int idBoleta, float boletaNeto, float boletaImpuesto, float boletaTotal, boolean boletaFormal, String boletaFechaModificacion) {
        this.idBoleta = idBoleta;
        this.boletaNeto = boletaNeto;
        this.boletaImpuesto = boletaImpuesto;
        this.boletaTotal = boletaTotal;
        this.boletaFormal = boletaFormal;
        this.boletaFechaModificacion = boletaFechaModificacion;
    }

    public int getIdBoleta() {
        return idBoleta;
    }

    public void setIdBoleta(int idBoleta) {
        this.idBoleta = idBoleta;
    }

    public float getBoletaNeto() {
        return boletaNeto;
    }

    public void setBoletaNeto(float boletaNeto) {
        this.boletaNeto = boletaNeto;
    }

    public float getBoletaImpuesto() {
        return boletaImpuesto;
    }

    public void setBoletaImpuesto(float boletaImpuesto) {
        this.boletaImpuesto = boletaImpuesto;
    }

    public float getBoletaTotal() {
        return boletaTotal;
    }

    public void setBoletaTotal(float boletaTotal) {
        this.boletaTotal = boletaTotal;
    }

    public boolean isBoletaFormal() {
        return boletaFormal;
    }

    public void setBoletaFormal(boolean boletaFormal) {
        this.boletaFormal = boletaFormal;
    }

    public String getBoletaFechaModificacion() {
        return boletaFechaModificacion;
    }

    public void setBoletaFechaModificacion(String boletaFechaModificacion) {
        this.boletaFechaModificacion = boletaFechaModificacion;
    }
 




}
