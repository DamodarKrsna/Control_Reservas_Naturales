

package com.parque.entidades;


public class Ciudad {
    private int idCiudad;
    private String nombreCiudad;
    private int codigoPostal;
    
    public Ciudad (int idCiudad, String nombreCiudad, int codigoPostal) 
    {
        this.idCiudad = idCiudad;
        this.nombreCiudad = nombreCiudad;
        this.codigoPostal = codigoPostal;
    }
    
    //get and set idciudad
    public int getIdCiudad()
    {
        return idCiudad;
    }
    
    public void setIdCiudad(int idCiudad)
    {
        this.idCiudad = idCiudad;
    }
    //get and set nombreCiudad
    public String getNombreCiudad()
    {
        return nombreCiudad;
    }
    
    public void setNombreCiudad(String nombreCiudad)
    {
        this.nombreCiudad = nombreCiudad;
    }
    //get and set codigoPostal
    public int getCodigoPostal()
    {
        return codigoPostal;
    }
    
    public void setCodigoPostal(int codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }
    
    
    
}
