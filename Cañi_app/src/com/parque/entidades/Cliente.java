
package com.parque.entidades;


public class Cliente {
    private int idCliente;
    private String clienteNombre;
    private String clienteApellidoP;
    private String clienteApellidoM;
    private int clienteEdad;
    private int clienteRut;
    private String clienteSexo;

    public Cliente(int idCliente, String clienteNombre, String clienteApellidoP, String clienteApellidoM, int clienteEdad, int clienteRut, String clienteSexo) {
        this.idCliente = idCliente;
        this.clienteNombre = clienteNombre;
        this.clienteApellidoP = clienteApellidoP;
        this.clienteApellidoM = clienteApellidoM;
        this.clienteEdad = clienteEdad;
        this.clienteRut = clienteRut;
        this.clienteSexo = clienteSexo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getClienteNombre() {
        return clienteNombre;
    }

    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

    public String getClienteApellidoP() {
        return clienteApellidoP;
    }

    public void setClienteApellidoP(String clienteApellidoP) {
        this.clienteApellidoP = clienteApellidoP;
    }

    public String getClienteApellidoM() {
        return clienteApellidoM;
    }

    public void setClienteApellidoM(String clienteApellidoM) {
        this.clienteApellidoM = clienteApellidoM;
    }

    public int getClienteEdad() {
        return clienteEdad;
    }

    public void setClienteEdad(int clienteEdad) {
        this.clienteEdad = clienteEdad;
    }

    public int getClienteRut() {
        return clienteRut;
    }

    public void setClienteRut(int clienteRut) {
        this.clienteRut = clienteRut;
    }

    public String getClienteSexo() {
        return clienteSexo;
    }

    public void setClienteSexo(String clienteSexo) {
        this.clienteSexo = clienteSexo;
    }
    
    
    
    
    
    
    
}
