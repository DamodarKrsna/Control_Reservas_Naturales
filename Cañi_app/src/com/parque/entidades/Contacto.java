
package com.parque.entidades;


public class Contacto {
    private int idContacto;
    private int contactoTelefonoPersonal;
    private int contactoTelefonoEmergencia;
    private String contactoCorreo;

    public Contacto(int idContacto, int contactoTelefonoPersonal, int contactoTelefonoEmergencia, String contactoCorreo) {
        this.idContacto = idContacto;
        this.contactoTelefonoPersonal = contactoTelefonoPersonal;
        this.contactoTelefonoEmergencia = contactoTelefonoEmergencia;
        this.contactoCorreo = contactoCorreo;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public int getContactoTelefonoPersonal() {
        return contactoTelefonoPersonal;
    }

    public void setContactoTelefonoPersonal(int contactoTelefonoPersonal) {
        this.contactoTelefonoPersonal = contactoTelefonoPersonal;
    }

    public int getContactoTelefonoEmergencia() {
        return contactoTelefonoEmergencia;
    }

    public void setContactoTelefonoEmergencia(int contactoTelefonoEmergencia) {
        this.contactoTelefonoEmergencia = contactoTelefonoEmergencia;
    }

    public String getContactoCorreo() {
        return contactoCorreo;
    }

    public void setContactoCorreo(String contactoCorreo) {
        this.contactoCorreo = contactoCorreo;
    }
    
    
    
    
}
