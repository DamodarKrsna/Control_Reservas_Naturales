
package com.parque.entidades;


public class Escuela {
    
    private int idescuela;
    private String nombreEscuela;
    private int añosCursados;
    private String carrera;
    private String timpoPractica;

    public Escuela(int idescuela, String nombreEscuela, int añosCursados, String carrera, String timpoPractica) {
        this.idescuela = idescuela;
        this.nombreEscuela = nombreEscuela;
        this.añosCursados = añosCursados;
        this.carrera = carrera;
        this.timpoPractica = timpoPractica;
    }

    public String getTimpoPractica() {
        return timpoPractica;
    }

    public void setTimpoPractica(String timpoPractica) {
        this.timpoPractica = timpoPractica;
    }

    public int getIdescuela() {
        return idescuela;
    }

    public void setIdescuela(int idescuela) {
        this.idescuela = idescuela;
    }

    public String getNombreEscuela() {
        return nombreEscuela;
    }

    public void setNombreEscuela(String nombreEscuela) {
        this.nombreEscuela = nombreEscuela;
    }

    public int getAñosCursados() {
        return añosCursados;
    }

    public void setAñosCursados(int añosCursados) {
        this.añosCursados = añosCursados;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    
    
    
    
    
    
}
