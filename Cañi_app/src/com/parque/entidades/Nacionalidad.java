
package com.parque.entidades;


public class Nacionalidad {
    private int idNacionalidad;
    private String nacionalidadPais;
    private String nacionalidadContinente;

    public Nacionalidad(int idNacionalidad, String nacionalidadPais, String nacionalidadContinente) {
        this.idNacionalidad = idNacionalidad;
        this.nacionalidadPais = nacionalidadPais;
        this.nacionalidadContinente = nacionalidadContinente;
    }

    public int getIdNacionalidad() {
        return idNacionalidad;
    }

    public void setIdNacionalidad(int idNacionalidad) {
        this.idNacionalidad = idNacionalidad;
    }

    public String getNacionalidadPais() {
        return nacionalidadPais;
    }

    public void setNacionalidadPais(String nacionalidadPais) {
        this.nacionalidadPais = nacionalidadPais;
    }

    public String getNacionalidadContinente() {
        return nacionalidadContinente;
    }

    public void setNacionalidadContinente(String nacionalidadContinente) {
        this.nacionalidadContinente = nacionalidadContinente;
    }
    
    
    
    
    
    
    
}
