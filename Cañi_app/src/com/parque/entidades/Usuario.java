
package com.parque.entidades;


public class Usuario {
    
    private int idUsuario;
    private String usuarioUsuario;
    private String usuarioPaswword;
    private boolean usuarioAdministrador;
    private String usuarioNombre;
    private String usuarioApellidoPaterno;
    private String usuarioApellidoMaterno;
    private String usuarioRut;
    private String usuarioSexo;
    private int usuarioEdad;
    private boolean usuariolanta;
    private String usuarioFechaModificacion;

    public Usuario(int idUsuario, String usuarioUsuario, String usuarioPaswword, boolean usuarioAdministrador, String usuarioNombre, String usuarioApellidoPaterno, String usuarioApellidoMaterno, String usuarioRut, String usuarioSexo, int usuarioEdad, boolean usuariolanta, String usuarioFechaModificacion) {
        this.idUsuario = idUsuario;
        this.usuarioUsuario = usuarioUsuario;
        this.usuarioPaswword = usuarioPaswword;
        this.usuarioAdministrador = usuarioAdministrador;
        this.usuarioNombre = usuarioNombre;
        this.usuarioApellidoPaterno = usuarioApellidoPaterno;
        this.usuarioApellidoMaterno = usuarioApellidoMaterno;
        this.usuarioRut = usuarioRut;
        this.usuarioSexo = usuarioSexo;
        this.usuarioEdad = usuarioEdad;
        this.usuariolanta = usuariolanta;
        this.usuarioFechaModificacion = usuarioFechaModificacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuarioUsuario() {
        return usuarioUsuario;
    }

    public void setUsuarioUsuario(String usuarioUsuario) {
        this.usuarioUsuario = usuarioUsuario;
    }

    public String getUsuarioPaswword() {
        return usuarioPaswword;
    }

    public void setUsuarioPaswword(String usuarioPaswword) {
        this.usuarioPaswword = usuarioPaswword;
    }

    public boolean isUsuarioAdministrador() {
        return usuarioAdministrador;
    }

    public void setUsuarioAdministrador(boolean usuarioAdministrador) {
        this.usuarioAdministrador = usuarioAdministrador;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getUsuarioApellidoPaterno() {
        return usuarioApellidoPaterno;
    }

    public void setUsuarioApellidoPaterno(String usuarioApellidoPaterno) {
        this.usuarioApellidoPaterno = usuarioApellidoPaterno;
    }

    public String getUsuarioApellidoMaterno() {
        return usuarioApellidoMaterno;
    }

    public void setUsuarioApellidoMaterno(String usuarioApellidoMaterno) {
        this.usuarioApellidoMaterno = usuarioApellidoMaterno;
    }

    public String getUsuarioRut() {
        return usuarioRut;
    }

    public void setUsuarioRut(String usuarioRut) {
        this.usuarioRut = usuarioRut;
    }

    public String getUsuarioSexo() {
        return usuarioSexo;
    }

    public void setUsuarioSexo(String usuarioSexo) {
        this.usuarioSexo = usuarioSexo;
    }

    public int getUsuarioEdad() {
        return usuarioEdad;
    }

    public void setUsuarioEdad(int usuarioEdad) {
        this.usuarioEdad = usuarioEdad;
    }

    public boolean isUsuariolanta() {
        return usuariolanta;
    }

    public void setUsuariolanta(boolean usuariolanta) {
        this.usuariolanta = usuariolanta;
    }

    public String getUsuarioFechaModificacion() {
        return usuarioFechaModificacion;
    }

    public void setUsuarioFechaModificacion(String usuarioFechaModificacion) {
        this.usuarioFechaModificacion = usuarioFechaModificacion;
    }
    
    
    
    
    
    
}
