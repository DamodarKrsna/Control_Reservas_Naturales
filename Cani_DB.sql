create table ESCUELA(
	"ID_ESCUELA" int not null  GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"escuelaNnombre" varchar(20),
	"escuelaAñosCursados" int,
	"escuelaCarrera" varchar(15),
	"escuelaTiempoPractica" date,
	primary key ("ID_ESCUELA")
);

create table REGION(
	"ID_REGION" int not null  GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"regionNombre" varchar(15),
	"regionNumero" int,
	primary key ("ID_REGION")
);

create table CIUDAD(
	"ID_CIUDAD" int not null  GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"ciudadNombre" varchar(20),
	"ciudadCodigoPortal" int,
	"FK_ID_REGION_REGION_CIUDAD"int ,
	primary key ("ID_CIUDAD"),
	foreign key ("FK_ID_REGION_REGION_CIUDAD") references REGION ("ID_REGION")
);

create table CALLE(
	"ID_CALLE" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"calleNombre" varchar(20),
	"calleNumero" int,
	"FK_ID_CIUDAD_CIUDAD_CALLE" int,
	primary key ("ID_CALLE"),
	foreign key ("FK_ID_CIUDAD_CIUDAD_CALLE") references CIUDAD ("ID_CIUDAD")
);

create table NACIONALIDAD(
	"ID_NACIONALIDAD" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"nacionalidadPais" varchar(20),
	"nacionalidadContinente" varchar(15),
	"FK_ID_REGION_REGION_NACIONALIDAD" int,
	"FK_ID_CIUDAD_CIUDAD_NACIONALIDAD" int,
	"FK_ID_CALLE_CALLE_NACIONALIDAD" int,
	primary key ("ID_NACIONALIDAD")
);

create table CLIENTE(
	"ID_CLIENTE" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"clienteNombre" varchar(20),
	"clienteApellidoPaterno" varchar(20),
	"clienteApellidoMaterno" varchar(20),
	"clienteEdad" int,
	"clienteRut" int,
	"clienteSexo" varchar(10),
	"clienteUltimaFechaModificacion" date, 
	"FK_ID_NACIONALIDAD_NACIONALIDAD_CLIENTE" int,
	"FK_ID_REGION_REGION_CLIENTE" int,
	"FK_ID_CIUDAD_CIUDAD_CLIENTE" int,
	"FK_ID_CALLE_CALLE_CLIENTE" int,
	primary key ("ID_CLIENTE"),
	foreign key ("FK_ID_NACIONALIDAD_NACIONALIDAD_CLIENTE") references NACIONALIDAD("ID_NACIONALIDAD"),
	foreign key ("FK_ID_REGION_REGION_CLIENTE") references REGION ("ID_REGION"),
	foreign key ("FK_ID_CIUDAD_CIUDAD_CLIENTE") references CIUDAD ("ID_CIUDAD"),
	foreign key ("FK_ID_CALLE_CALLE_CLIENTE") references CALLE ("ID_CALLE")
);

create table USUARIO(
	"ID_USUARIO" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"usuarioUsuario" varchar(20),
	"usuarioPassword" varchar(20),
	"usuarioAdmin" BOOLEAN,
	"usuarioNombre" varchar(20),
	"usuarioApellidpPaterno" varchar(20),
	"usuarioApellidpMaterno" varchar(20),
	"usuarioRut" varchar(12),
	"usuarioSexo" varchar(10),
	"usuarioEdad" int,
	"usuarioPlanta" BOOLEAN,
	"usuarioUltimaFechaModificacion" date, 
	"FK_ID_ESCUELA_ESCUELA" int,
	"FK_ID_NACIONALIDAD_NACIONALIDAD_USUARIO" int,
	"FK_ID_REGION_REGION_USUARIO" int,
	"FK_ID_CIUDAD_CIUDAD_USUARIO" int,
	"FK_ID_CALLE_CALLE_USUARIO" int,
	primary key ("ID_USUARIO"),
	foreign key ("FK_ID_NACIONALIDAD_NACIONALIDAD_USUARIO") references NACIONALIDAD("ID_NACIONALIDAD"),
	foreign key ("FK_ID_REGION_REGION_USUARIO") references REGION("ID_REGION"),
	foreign key ("FK_ID_CIUDAD_CIUDAD_USUARIO") references CIUDAD("ID_CIUDAD"),
	foreign key ("FK_ID_CALLE_CALLE_USUARIO") references  CALLE("ID_CALLE")
);

create table RESERVA(
	"ID_RESERVA" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"reservaCheckin" date,
	"reservaChackout" date,
	"reservaNochesEstadia" int,
	"reservaGuiado" BOOLEAN,
	"reservaUltimaFechaModificacion" date, 
	"FK_ID_CLIENTE_CLIENTE_RESERVA" int,
	"FK_ID_USUARIO_USUARIO_RESERVA" int,
	primary key ("ID_RESERVA"),
	foreign key ("FK_ID_CLIENTE_CLIENTE_RESERVA") references CLIENTE ("ID_CLIENTE"),
	foreign key ("FK_ID_USUARIO_USUARIO_RESERVA") references USUARIO ("ID_USUARIO")
);


create table CONTACTO(
	"ID_CONTACTO" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"contactoTelefonoPersonal" int,
	"contactoTelefonoEmergencia" int,
	"contactoCorreo" varchar(20),
	"FK_ID_NACIONALIDAD_NACIONALIDAD_CONTACTO" int,
	"FK_ID_CLIENTE_CLIENTE_CONTACTO" int,
	"FK_ID_USUARIO_USUARIO_CONTACTO" int,
	primary key ("ID_CONTACTO"),
	foreign key ("FK_ID_CLIENTE_CLIENTE_CONTACTO") references CLIENTE ("ID_CLIENTE"),
	foreign key ("FK_ID_USUARIO_USUARIO_CONTACTO") references USUARIO ("ID_USUARIO")
);

create table BOLETA(
	"ID_BOLETA" int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"boletaNeteo" int,
	"boletaImpuesto" int,
	"boletaTotal" int,
	"boletaFormal" BOOLEAN,
	"boeletaTipoPago" varchar(16),
	"boletaUltimaFechaModificacion" date, 
	"FK_ID_USUARIO_USUARIO_BOLETA" int,
	"FK_ID_CLIENTE_CLIENTE_BOLETA" int,
	"FK_ID_RESERVA_RESERVA_BOLETA" int,
	primary key ("ID_BOLETA"),
    foreign key ("FK_ID_USUARIO_USUARIO_BOLETA") references  USUARIO ("ID_USUARIO"),
    foreign key ("FK_ID_CLIENTE_CLIENTE_BOLETA") references CLIENTE ("ID_CLIENTE")
);

























